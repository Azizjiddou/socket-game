import socket
import random
import time

def demarrer_serveur():
    # Création de la socket du serveur
    serveur_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Liaison de la socket à l'adresse localhost et au port 65432
    serveur_socket.bind(('localhost', 65432))
    
    # Mise en écoute de la socket, prête à accepter des connexions entrantes
    serveur_socket.listen()

    print("Bienvenue au jeu de la devinette !")
    
    # Acceptation d'une connexion entrante
    connexion, adresse = serveur_socket.accept()
    
    with connexion:
        print(f"Joueur {adresse} connecté")

        # Envoi du message initial au client
        connexion.sendall(b"Deviner un nombre entre 1 et 10")

        # Initialisation du compteur d'essais et du temps de début
        while True:
            nombre_gagnant = random.randint(1, 10)
            print(f"Le nombre à deviner est : {nombre_gagnant}")

            nombre_essais = 0
            debut_temps = time.time()
            while True:
                # Réception des données du client. 1024 est le nombre d'octet à ne pas depasser. c'est le standard dans la programmation via les sockets.

                donnees = connexion.recv(1024)
                if not donnees:
                    break

                nombre_essais += 1

                try:
                    # Décodage et conversion de la donnée reçue en entier
                    devinette = int(donnees.decode())
                    if devinette == nombre_gagnant:
                        # Si le client a deviné correctement
                        fin_temps = time.time()
                        temps_ecoule = fin_temps - debut_temps
                        reponse = f"Bien joué ! Nombre d'essais : {nombre_essais}, Temps écoulé : {temps_ecoule:.2f} secondes"
                        print(f"Le joueur {adresse} a gagné")

                        # Envoi de la réponse au client
                        connexion.sendall(reponse.encode())
                        break
                    else:
                        # Si le client n'a pas deviné correctement
                        print(f"{devinette} n'est pas le bon nombre")
                        reponse = "Incorrect. Essayez à nouveau !"

                except ValueError:
                    # Si la donnée reçue n'est pas valide
                    reponse = "Veuillez saisir uniquement un nombre entre 1 et 10"
                
                connexion.sendall(reponse.encode())

            # On attend de voir si le client veut rejouer
            donnees = connexion.recv(1024)
            if not donnees:
                break


    # Fermeture de la socket du serveur
    serveur_socket.close()

if __name__ == "__main__":
    demarrer_serveur()
