To play the game clone the project with 

'''git clone https://gitlab.com/Azizjiddou/socket-game'''

You will need python3 installed on both client and server

The only libs used are socket, random and time which should come with python3 by default.

In one terminal start the server with the following command

```python3 server.py```

Open another terminal an launch the client with the following command

```python3 client.py```

If you wish to play the game on different machines, be sure to be on the same network and update the socket address and port on both server and client files
