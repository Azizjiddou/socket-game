import socket

def demarrer_client():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('localhost', 65432))
    firstGame = True
    while True:
        jouer_jeu(client_socket, firstGame)

        # Demande au client s'il veut rejouer
        rejouer = input("Voulez-vous rejouer ? (oui/non) ").lower().strip() in ["oui", "o"]
        if rejouer :
            client_socket.sendall("OK".encode())
            firstGame = False
        else:
            print("Merci d'avoir joué !")
            client_socket.close()
            return

def jouer_jeu(socket, firstGame):
    while True:
        if firstGame:
            # Réception du message initial du serveur
            donnees = socket.recv(1024)
            if not donnees:
                break
            print(donnees.decode())

        while True:
            devinette = input("Saisissez un nombre : ")
            
            # Envoi du nombre deviné au serveur
            socket.sendall(devinette.encode())

            # Réception de la réponse du serveur
            reponse = socket.recv(1024)
            print(reponse.decode())

            # Si la réponse indique que le nombre a été correctement deviné
            if "Bien joué !" in reponse.decode():
                return


if __name__ == "__main__":
    demarrer_client()
